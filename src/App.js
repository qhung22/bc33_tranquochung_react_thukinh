// import logo from "./logo.svg";
import "./App.css";
import BaiTapThuKinh from "./BaiTapThuKinh/BaiTapThuKinh";
import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";

import DataBinding from "./DataBinding/DataBinding";
import DemoState from "./DemoState/DemoState";
import EvenBinding from "./EventBinding/EvenBinding";

import Ex_Layout_1 from "./Ex_Layout_1/Ex_Layout_1";
import Ex_Layout_2 from "./Ex_Layout_2/Ex_Layout_2";
import RenderWithMap from "./RenderWithMap/RenderWithMap";

function App() {
  return (
    <div className="App">
      {/* <DemoClass /> */}
      {/* <DemoFunction /> */}
      {/* <Ex_Layout_1 /> */}
      {/* <Ex_Layout_2/> */}
      {/* <DataBinding /> */}
      {/* <EvenBinding/> */}
      {/* <ConditionalRendering /> */}
      {/* <DemoState /> */}
      {/* <div className="title">heloo app</div>
      <RenderWithMap /> */}
      <BaiTapThuKinh />
    </div>
  );
}

export default App;
