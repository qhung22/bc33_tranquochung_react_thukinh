import React, { Component } from "react";
import dataGlasses from "../DaTa/dataGlasses";
import { data } from "../DaTa/dataGlasses";
export default class BaiTapThuKinh extends Component {
  state = {
    glasses: data,
    model: {
      name: "GUCCI G8850U",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
      url: "./glassesImage/v1.png",
    },
  };

  handleChangeGlass = (data) => {
    console.log("data: ", data);
    this.setState({
      model: {
        name: data.name,
        desc: data.desc,
        url: data.url,
      },
    });
  };

  renderGlassesList = () => {
    return this.state.glasses.map((glassesItem, index) => {
      console.log("glassesItem: ", glassesItem);
      return (
        <div className="col-4">
          <img
            key={index}
            onClick={() => this.handleChangeGlass(glassesItem)}
            src={glassesItem.url}
            alt=""
            width={300}
            height={100}
            style={{ padding: "20px 50px", cursor: "pointer" }}
          />
        </div>
      );
    });
  };

  render() {
    const styleGlasses = {
      width: "150px",
      top: "70px",
      right: "85px",
      opacity: 0.7,
    };
    const styleInfo = {
      width: "250px",
      top: "241px",
      left: "270px",
      backgroundColor: "rgba(127, 125, 0, .5)",
      textAlign: "left",
    };
    return (
      <div
        style={{
          backgroundImage: "url(./glassesImage/background.jpg)",
          backgroundSize: "1500px",
          minHeight: "1500px",
        }}
      >
        <div style={{ backgroundColor: "rgba(0,0,0,.3)", minHeight: "1500px" }}>
          <h3 style={{ backgroundColor: "rgba(0,0,0,.3)" }}>
            {" "}
            THỬ KÍNH ĐÊ BẠN ỚI
          </h3>
          <div className="container">
            <div className="row mt-5">
              <div className="col-6">
                <div className="position-relative"></div>
                <img
                  className="position-absolute"
                  width={250}
                  src="./glassesImage/model.jpg"
                  alt=""
                />
                <img
                  style={styleGlasses}
                  className="position-absolute"
                  src={this.state.model.url}
                  alt=""
                />
                <div className="position-relative" style={styleInfo}>
                  <p>Tên Kính : {this.state.model.name}</p>
                  <p>mô tả : {this.state.model.desc}</p>
                </div>
              </div>
              <div className="col-6">
                <img width={250} src="./glassesImage/model.jpg" alt="" />
              </div>
            </div>
          </div>
          //div chứa các kính
          <div className="row">{this.renderGlassesList()}</div>
        </div>
      </div>
    );
  }
}
