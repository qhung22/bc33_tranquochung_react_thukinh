import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  isLogin = false;
  handleLogin = () => {
    this.isLogin = true;
    console.log("this.isLogin: ", this.isLogin);
  };
  renderContent = () => {
    if (this.isLogin) {
      return (
        <>
          <p>Đã Đăng Nhập</p>
          <button onClick={this.handleLogin} className="btn btn-danger">
            Đăng Xuất
          </button>
        </>
      );
    }
    return (
      <>
        <p>Chưa Đăng Nhập</p>
        <button onClick={this.handleLogin} className="btn btn-primary">
          {" "}
          Đăng Nhâp
        </button>
      </>
    );
  };
  render() {
    return <div>{this.renderContent()}</div>;
    // return <div>{this.isLogin ? "Đã Đăng Nhập" : "Chưa Đăng Nhập"}</div>;
  }
}
