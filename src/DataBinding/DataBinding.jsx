import React, { Component } from "react";

export default class DataBinding extends Component {
  userAge = 2;
  renderFooter = () => {
    return <h1>Footer</h1>;
  };
  render() {
    let username = "Alice";
    let imgSrc =
      "https://cdn.longkhanhpets.com/2022/08/tam-ly-loai-meo-1-710x375.jpg";
    return (
      <div>
        Hello {username}
        <div>
          <p> userAge: {this.userAge}</p>
          <img src={imgSrc} alt="" style={{ width: 300, marginTop: 50 }} />
        </div>
        {this.renderFooter()}
        {this.renderFooter()}
        {this.renderFooter()}
        {this.renderFooter()}
      </div>
    );
  }
}
