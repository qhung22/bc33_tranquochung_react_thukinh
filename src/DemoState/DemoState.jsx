import React, { Component } from "react";

export default class DemoState extends Component {
  //state nơi lưu giữ dữ liệu liên quan đến component ~ thay đổi dữ liệu này thì cần tạo giao diện mới tương ứng
  state = {
    like: 0,
    userInfo: {
      name: "Alice",
    },
  };
  handlePlusLike = () => {
    //setState dùng để update giá trị của state
    this.setState({
      like: this.state.like + 20,
    });
  };
  handleChangeName = () => {
    this.setState({
      userInfo: {
        name: "Bob",
      },
    });
  };
  //state thay đổi thì render chạy lợi
  render() {
    console.log("chạy lại");
    return (
      <div>
        <p>{this.state.like}</p>
        <button onClick={this.handlePlusLike} className="btn btn-primary">
          plus like
        </button>
        <br />
        <p>User name : {this.state.userInfo.name}</p>
        <button onClick={this.handleChangeName} className="btn btn-danger">
          Change Alice To Bob
        </button>
      </div>
    );
  }
}
