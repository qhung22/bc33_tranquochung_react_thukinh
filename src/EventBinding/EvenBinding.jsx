import React, { Component } from "react";

export default class EvenBinding extends Component {
  //function k có tham số
  handleSayhello = () => {
    console.log("Hello");
  };
  handleSayhelloByName = (name) => {
    console.log("Hello " + name);
  };
  render() {
    return (
      <div>
        <button onClick={this.handleSayhello} className="btn btn-success">
          Say hello
        </button>
        <br />
        <button
          onClick={() => {
            this.handleSayhelloByName("Lâm bê");
          }}
          className="btn btn-danger"
        >
          Say hello to Alice
        </button>
      </div>
    );
  }
}
