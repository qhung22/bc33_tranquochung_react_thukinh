import React from "react";
import Content from "./Content";
import Footer from "./Footer";
import Header from "./Header";
import Home from "./Home";
import Nav from "./Nav";

export default function Ex_Layout_1() {
  return (
    <div>
      <Home />
      <Header />
      <div className="row">
        <div className="col-4 bg-success">
          <Nav />
        </div>
        <div className="col-8 bg-secondary">
          <Content />
        </div>
      </div>

      <Footer />
    </div>
  );
}
