import React, { Component } from "react";
import { data } from "./dataGlasses";
import styles from "./renderWithMap.module.css";
export default class RenderWithMap extends Component {
  state = {
    glasses: data,
  };
  
  handleChangeGlass = (data) => {
    console.log("data: ", data);N
  };
  renderList = () => {
    return this.state.glasses.map((item, index) => {
      console.log("item: ", item);
      return (
        <img
          key={index}
          onClick={() => this.handleChangeGlass(item)}
          style={{
            width: `${100 / this.state.glasses.length}%`,
            padding: "0px 15px",
          }}
          src={item.url}
          alt=""
        />
      );
    });
  };
  render() {
    return (
      <div>
        <div className={styles.title}>Helo tilte</div>
        {/* <img src="./glassesImage/g2.jpg" alt="" /> */}
        {this.renderList()}
      </div>
    );
  }
}
